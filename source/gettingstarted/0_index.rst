Getting Started: NetModule Linux
================================

NetModule AG provides an open source Linux Distribution based on
Yoctoproject's Reference distro "Poky".

If you are new into yoctoproject you might want to read this: 
`Getting Started: The Yocto Project Overview <https://www.yoctoproject.org/software-overview/>`_

NetModule Addons
----------------

Board Support Packages
  -  Layer `meta-netmodule-bsp`: Recipes to provide hardware support

NetModule Distro
  -  Layer `meta-netmodule-distro`: NetModule HW specific package selection

Integration of OSTree
  -  Layer `meta-updater`

Image Types
-----------

netmodule-linux-release
  -  OSTree support
  -  clean build, ready to deploy

netmodule-linux-dev
  -  OSTree support
  -  Several tools a developer would find useful already integrated.

netmodule-linux-minimal
  -  Minimal Kernel and Image size
  -  RAM Disk Support

