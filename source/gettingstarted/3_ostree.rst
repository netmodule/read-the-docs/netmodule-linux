OSTree
======

OSTree is a system for versioning updates of Linux-based operating
systems. It can be considered as "git for operating system binaries".

`OSTree Documentation <https://ostree.readthedocs.io/en/latest/>`_

Description
-----------

Difference between a "normal" OS and an "atomic" OS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

On a normal OS, the updates are handled by a package-manager. If an
update is executed, the package-manager updates each package to the
newest version available at the current time. This results in an unique
set of packages after every update.

When you do an update on an atomic OS, you update the OS as a whole,
giving you a specific set of packages every time.

Filesystem structure
~~~~~~~~~~~~~~~~~~~~

An operating system deployed with ostree is always consistent with the
"ostree commit". To make sure this is the case, the os has to be
immutable. Ostree does this by creating a read-only bind-mount of the
/usr folder.

OSTree uses `UsrMov <http://www.freedesktop.org/wiki/Software/systemd/TheCaseForTheUsrMerge/>`_,
this means that the folders "bin", "lib" and "sbin" are moved from the
root to the /usr directory. On the root those folders are replaced with
links to the new location under /usr. This is done, because it reduces
the need of read-only bind-mounts to the single /usr directory.

The root of an ostree-deployment is located in:
``/sysroot/ostree/deploy/<os-name>/deploy/<commit hash>``

System Partitioning
~~~~~~~~~~~~~~~~~~~

Because ostree mounts the /usr folder as read-only, we have two options
to add additional software:

1. Create a new partition on the eMMC and mount it on lets say /data. \|
   If done like this, new software can now be added to this data-folder.

2. Create an overlay over the /usr directory. If done like this, the
   /usr folder behaves like a normal read-write folder.
   This can be done with the command ``ostree admin unlock --hotfix``
   **Note: The changes done like this are reverted after another ostree update.**
   **Note: This is generally not recommended for production systems.**

Boot Sequence
~~~~~~~~~~~~~

1. u-boot loads a uEnv.txt file which contains:

+------------------+---------------------------------------------------------+
| variable         | contents                                                |
+==================+=========================================================+
| kernel\_image    | path to the kernel image                                |
+------------------+---------------------------------------------------------+
| ramdisk\_image   | path to the ramdisk image                               |
+------------------+---------------------------------------------------------+
| bootargs         | the bootargs containing the path to the ostree-root.    |
+------------------+---------------------------------------------------------+
| kernel\_image2   | path to the fallback kernel image                       |
+------------------+---------------------------------------------------------+
| ramdisk\_image2  | path to the fallback ramdisk image                      |
+------------------+---------------------------------------------------------+
| bootargs2        | the bootargs containing the path to the fallback        |
|                  | ostree-root.                                            |
+------------------+---------------------------------------------------------+

2. u-boot loads the kernel and ramdisk given by uEnv.txt
3. the ramdisk contains a script (/sbin/init) which prepares the rootfs.
4. the script runs ``pivot_root`` to switch from the ramdisk to the
   newly generated rootfs.
5. the script calls the /sbin/init of the new rootfs.

.. _ostree_update:

Update system with local ostree
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. Put the "*ostree\_repo*" folder on your http server: /srv/http/ostree_repo
2. If you are reusing the local directory you need to update the summary file.
   To do this, install ostree on the server (available in debian repositories)
   and type the following:

.. code-block:: bash

  ostree summary -u --repo=/srv/http/ostree_repo

2. On the target type:

.. code-block:: bash

  # Add the repo on the target
  ostree remote add local_repo http://<local_ip>/ostree_repo --no-gpg-verify
  # List all available "refs"
  ostree remote refs local_repo

  # Examples to deploy the dev image for HW26:
  ostree pull local_repo:dunfell-am335x-hw26-dev
  ostree admin deploy local_repo:dunfell-am335x-hw26-dev

verify pending update
.....................

::

  ostree admin status | grep pending

reboot to apply update
......................

::

  reboot

Update system with ostree via network
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. Connect to the network.

::

  nmcli c mod ethernet ipv4.method auto
  nmcli c up ethernet

2. Add the repositories.

::

  ostree remote add nmrepo-stable https://nmrepo.netmodule.com/chbe/stable/ --no-gpg-verify
  ostree remote add nmrepo-unstable https://nmrepo.netmodule.com/chbe/unstable/ --no-gpg-verify

3. Download the image.

There are multiple architectures and images available.
The naming convention is:

``{YOCTO_VERSION}-{MACHINE}-{IMAGE_TYPE}``
e.g. ``dunfell-am335x-nmhw21-vcu``

Do only one of the following commands.

::

  # Do this to get the newest stable image.
  ostree pull nmrepo-stable dunfell-am335x-nmhw21-vcu
  # Do this to get the newest unstable / nightly image.
  ostree pull nmrepo-unstable dunfell-am335x-nmhw21-vcu


4. On the terminal type:

Do only one of the following commands.

::

  # Do this to deploy the newest stable image.
  ostree admin deploy nmrepo-stable:dunfell-am335x-nmhw21-vcu
  # Do this to deploy the newest unstable / nightly image.
  ostree admin deploy nmrepo-unstable:dunfell-am335x-nmhw21-vcu


