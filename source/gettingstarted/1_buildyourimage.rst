Build Your Image
================

.. role:: bash(code)
   :language: bash

NetModule provides its own meta layer for customized image generation.

Project Setup
-------------

Netmodule Meta Layer
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

+------------------------+---------------------------------------------------------+----------------------------------------------------------------------------------------------+
| Meta Layer             | Description                                             | Repository                                                                                   |
+========================+=========================================================+==============================================================================================+
| meta-netmodule-distro  | - Distribution specfic contents                         | `meta-netmodule-distro <https://gitlab.com/netmodule/yoctoproject/meta-netmodule-distro>`_   |
|                        | - Distribution configurations                           |                                                                                              |
|                        | - NetModule image recipes                               |                                                                                              |
|                        | - Firmware update tool                                  |                                                                                              |
|                        | - Disk expansion tool                                   |                                                                                              |
|                        | - General applications recipes                          |                                                                                              |
|                        | - Example local.conf file                               |                                                                                              |
+------------------------+---------------------------------------------------------+----------------------------------------------------------------------------------------------+
| meta-netmodule-bsp     | - Distribution specific contents                        | `meta-netmodule-bsp <https://gitlab.com/netmodule/yoctoproject/meta-netmodule-bsp>`_         |
|                        | - Machine confguration files                            |                                                                                              |
|                        | - U-Boot recipes                                        |                                                                                              |
|                        | - Linux kernel recipe                                   |                                                                                              |
|                        | - Applications recipes for interface support            |                                                                                              |
|                        | - General low level applications recipes                |                                                                                              |
+------------------------+---------------------------------------------------------+----------------------------------------------------------------------------------------------+
| meta-netmodule-wlan    | - Wlan module firmware                                  | `meta-netmodule-wlan <https://gitlab.com/netmodule/yoctoproject/meta-netmodule-wlan>`_       |
|                        | - Wireless regulatory databases                         |                                                                                              |
|                        | - WPA supplicant                                        |                                                                                              |
|                        | - Wireless tools                                        |                                                                                              |
+------------------------+---------------------------------------------------------+----------------------------------------------------------------------------------------------+

Additional Layers and Bitbake
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Following additional resources are required:

- `meta-openembedded <https://git.openembedded.org/meta-openembedded/>`_
- `meta-updater <https://github.com/advancedtelematic/meta-updater.git>`_
- `bitbake <https://git.openembedded.org/bitbake>`_
- `openembedded-core <https://git.openembedded.org/openembedded-core>`_

Setup workspace
~~~~~~~~~~~~~~~

Start new build project with cloning all required git repositories:

.. code-block:: bash

   # Clone the main repo
   git clone https://gitlab.com/netmodule/yoctoproject/nm-oem-linux.git
   cd nm-oem-linux
   # Initialize the submodules (meta-layers)
   git submodule init && git submodule update


Your workspace should now look similar as in the snippet below:

.. code-block:: none

   .
   ├── bitbake
   ├── build
   ├── env
   ├── meta-netmodule-bsp
   ├── meta-netmodule-distro
   ├── meta-netmodule-wlan
   ├── meta-openembedded
   ├── meta-updater
   ├── openembedded-core
   └── README.md


Configure project
~~~~~~~~~~~~~~~~~

To build a yocto image, two base configurations are required: the MACHINE and the DISTRO.
Both are provided from the shell environment and can be set by sourcing the required files:

.. code-block:: bash

   # Choose the OSTree distro
   source ./env/distro/ostree-image

   # List the available machines with
   ls ./env/machine/
   # Source the right one
   source ./env/machine/hw26


Once both are selected, the bitbake environment will be loaded and ready to build.


Build NetModule reference images
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

NetModule provides several reference images depends on use case:

============================= ==================================================
Image                         Description
============================= ==================================================
netmodule-linux-image         Standard reference image. Contains required
                              applications, drivers and tools to use interfaces,
                              network connectivities and sensors.
----------------------------- --------------------------------------------------
netmodule-linux-image-dev     Based on ``netmodule-linux-image`` and extended
                              with helpful development tools for low level
                              access (e.g. direct access on i2c bus) and generic
                              debug tools like strace and gdb.
----------------------------- --------------------------------------------------
netmodule-linux-image-minimal Minimal ramdisk based image for simple bring up or
                              can be used for emmc operations like device
                              flashing or data recovery.
============================= ==================================================


Images recipes location 
.......................

Images recipes are located in layer ``meta-netmodule-distro``:

.. code-block:: none

   meta-netmodule-distro/recipes-core/images/
   ├── netmodule-linux-image.bb
   ├── netmodule-linux-image-dev.bb
   └── netmodule-linux-image-minimal.bb

.. _sstate-usage:

Building with shared state
..........................

It is highly recommended to use the yocto shared state from NetModule to speed
up build time. Also some closed source tools like "lpa" will only be available
through sstate. The sstate will provide already built packages directly from
our servers as long as the recipes configurations are the same.

To build using our sstate, make sure that all the layers are matching the
submodule hashes from our releases and make sure that the following line is
present in your local.conf:

.. code-block:: none

   SSTATE_MIRRORS = "file://.* https://nmrepo.netmodule.com/chbe/oem-linux-sstate/PATH"


Images build instructions
.........................

Start image build after sourcing environment with:

::

	bitbake netmodule-linux-image


Images deploy location
.........................
Built images are located in deploy directory.::
	<project root>/build/tmp/deploy/images/<hw type e.g. am335x-nmhw21>/

If you built an ostree image, you can copy the content of the directory
*ostree_repo* to your http server and continue with :ref:`ostree_update`

.. code-block:: bash

   cp -r ostree_repo/* /srv/http/ostree_repo

Otherwise you can install the .wic file (full image for the mmc), following
the instructions at :ref:`image_installation`
