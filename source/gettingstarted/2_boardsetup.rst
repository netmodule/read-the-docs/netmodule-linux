Setup your Board
================

.. _image_installation:

Image Installation
------------------

Over the Network
~~~~~~~~~~~~~~~~

Note: This procedure will remove all contents on the eMMC. Make sure to backup your data!

Setup a HTTP server
...................

1. Install python3

::

   sudo apt install python3

2. Set your ip to 192.168.1.254 (If you use a network-manager set your static ip there! This command will not work in this case.)

::

   sudo ifconfig <Your ethernet device e.g. eth0> 192.168.1.254

3. Make a new folder and enter it

::

   sudo mkdir /srv/http
   cd /srv/http

4. Start the http-server

::

   sudo python3 -m http.server --bind 192.168.1.254 80


Setup a TFTP server
...................

1. Install the following packages

::

   sudo apt-get install xinetd tftpd tftp

2. Edit or make a file in: ``/etc/xinetd.d/tftp`` with the following content:

::

       service tftp
       {
       protocol        = udp
       port            = 69
       socket_type     = dgram
       wait            = yes
       user            = nobody
       server          = /usr/sbin/in.tftpd
       server_args     = -s /srv/tftp
       disable         = no
       }

3. Create a tftp folder

::

   mkdir -p /srv/tftp

4. Restart the xinetd service

::

   sudo service xinetd restart


Instructions to flash a device from scratch
...........................................

1. Put the WIC file (e.g. `image-am335x-nmhw21.wic`) file on the http-server (`/srv/http`).

2. Put the Minimal Linux fitImage (e.g. `fitImage-am335x-nmhw21.bin`) on the tftp-server (`/srv/tftp`).

3. Verify the `/srv` folder:

::

        /srv
        |-- http
        |   |-- image-am335x-nmhw21.wic
        |-- tftp
        .   |-- fitImage-netmodule-linux-image-minimal-am335x-nmhw21

4. Power up the board and press 's' on the serial terminal to stop in u-boot.

5. Validate that you are in u-boot console by typing

::

  # env print serverip
  serverip=192.168.1.254

6. Load kernel binary into ram

::

  # tftp $ramdisk_addr_r fitImage-netmodule-linux-image-minimal-am335x-nmhw21

7. restrict positioning of initrd ramdisk image

::

  # setenv initrd_high 0x84000000

8. Set boot args

::

  # setenv bootargs root=/dev/ram0 console=ttyS2,115200 ti_cpsw.rx_packet_max=1526

9. Boot from ramdisk. Linux will boot to login prompt within < 30s

::

  # bootm $ramdisk_addr_r

10. Login as `root` (empty password).

11. Disable kernel messages in Linux system

::

  echo 1 >  /proc/sys/kernel/printk

12. Burn the *.wic* file to the eMMC: (this takes about 5 min)

::

  curl -f http://192.168.1.254/image-am335x-nmhw21.wic | dd of=/dev/mmcblk1 bs=10M && sync

13. Reboot your system.

::

  reboot


From USB Stick
~~~~~~~~~~~~~~

Prepare USB Stick
.................

Format a USB-pendrive to have a with **fat** with a **maximum partition size of 4.0GB** for the first partition.

Use ``gnome-disks`` or `gparted`` for this.

The output of `parted -l` should look like this:

::

    Model: Kingston DataTraveler 3.0 (scsi)
    Disk /dev/sdb: ...GB
    Sector size (logical/physical): 512B/512B
    Partition Table: msdos
    Disk Flags:

    Number  Start   End       Size     Type     File system  Flags
    1       1049kB  4001MB    4000MB   primary  fat32        lba

1. Put the *image-am335x-nmhw21.wic* file on a USB-pendrive.

2. Put the files *image-minimal-am335x-nmhw21.cpio.gz.u-boot,
   fitImage-am335x-nmhw21.bin* on the USB-pendrive.

3. Your USB-pendrive should no look like this:

::

  USB-root
  |-- image-am335x-nmhw21.wic
  |-- image-minimal-am335x-nmhw21.cpio.gz.u-boot
  |-- fitImage-am335x-nmhw21.bin


Flash from USB Stick
....................

1. Plug the USB-pendrive into the nmhw21.

2. Power up the board and press 's' on the serial terminal to stop in
   u-boot.

3. Boot the ramdisk (copy-paste into u-boot terminal):

::

  usb reset; fatload usb 0:1 $kernel_addr_r fitImage-am335x-nmhw21.bin; fatload usb 0:1 $ramdisk_addr_r image-minimal-am335x-nmhw21.cpio.gz.u-boot; setenv bootargs root=/dev/ram0 console=ttyS2,115200 ti_cpsw.rx_packet_max=1526; bootm $kernel_addr_r $ramdisk_addr_r

4. When the the system has booted, log in with user `root`.

5. Mount the USB-pendrive

::

  mount /dev/sda1 /mnt

6. Burn the *.wic* file to the eMMC: (this takes about 5 min)

::

   dd if=/mnt/image-am335x-nmhw21.wic of=/dev/mmcblk1 bs=10M && sync

7. Reboot your system

::

   reboot

From SD Card
~~~~~~~~~~~~
