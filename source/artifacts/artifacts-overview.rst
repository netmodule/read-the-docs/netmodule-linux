Artifacts overview
==================

wic-file
--------

The wic file is a flashable image file. When upgrading a system with a
wic-file, the contents of the wic-file are mirrored to the mass storage
device.

Kickstart/wks-file
------------------

The wks-file defines where the contents are placed in the wic-file. In our yocto build-system the file is located in `meta-netmodule-bsp/wic/*.wks`.

More Information: `Yocto Project Reference  Manual <https://www.yoctoproject.org/docs/2.5/ref-manual/ref-manual.html#ref-kickstart>`_
