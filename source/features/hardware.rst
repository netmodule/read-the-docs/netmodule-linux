Supported devices
=================

NG800
-----

-  *Telematic Control Unit*
-  Interfaces: BroadR, CAN, Ethernet, Wi-Fi, LTE, GNSS, BT
-  Interna interface for user modules: Ethernet, USB, SPI, I2C, GPIO
-  CPU: TI am335x, 1000MHz
-  BSP: hw26

`Product Page NG800 <https://www.netmodule.com/en/products/router/ng800-lwwtgd2br2cm>`_

NB800
-----

-  *Customer-specific OEM Router*
-  Interfaces: LTE, Wi-Fi, Bluetooth and BLE
-  CPU: TI am335x, 600MHz
-  BSP: nrhw16, nrhw24

`Product Page NB800 <https://www.netmodule.com/en/products/router?routerLine=517>`_

NB1601
------

-  *Ruggedized OEM Router*
-  Interfaces: LTE, WiFi, GNSS and 4x Ethernet
-  CPU: TI am335x, 600MHz
-  BSP: nrhw20

`Product Page NB1601 <http://www.netmodule.com/products/industrial-routers/NB1601-LWWtSc-G.html>`_

NB1800
------

-  BSP: nrhw18
