am335x Startup
==============

.. image:: ../images/am335x-startup.png
   :alt: am335x-startup

am335x-startup

+------------------------------+------------------------------+
| HW                           | \*list                       |
+==============================+==============================+
| nmhw21                       | MMC1, MMC0, UART0, USB0      |
+------------------------------+------------------------------+
| nmhw21 (jumper on X200)      | UART0, XIP, MMC0, NAND       |
+------------------------------+------------------------------+

***See also:***
`AM335x Technical Reference Manual <http://www.ti.com/lit/ug/spruh73p/spruh73p.pdf>`_
-> Chapter 26 - Initialization (S. 5014)
