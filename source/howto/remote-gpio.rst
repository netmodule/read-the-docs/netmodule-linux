Remote GPIO driver
==================

Configuration
-------------

By default, driver will create 32 GPIO’s (named RGPIO0-RGPIO31), and
will set server IP and port as 192.168.1.42:6666.

It is possible, however, to configure Remote GPIO driver via device
tree:

::

  remote-gpios {
    compatible = "remote-gpios";

    ip = "192.168.1.64";
    port = /bits/ 16 <6666>;

    remote-gpio@0 {
      label = "umgpo0";
    };
    remote-gpio@1 {
      label = "umgpo1";
    };
    remote-gpio@2 {
      label = "umgpo2";
    };
    remote-gpio@3 {
      label = "umgpo3";
    };

    remote-gpio@4 {
      label = "umpu0";
    };
    remote-gpio@5 {
      label = "umpu1";
    };
    remote-gpio@6 {
      label = "umpu2";
    };
    remote-gpio@7 {
      label = "umpu3";
    };

  };


The ip-address and port can be changed via SysFS under ``/sys/class/remote-gpio/remote-gpio/config/``. The service "um-service-config.service" will configure the port to either "6666" or "7020" depending on the user-module firmware revision.

Protocol
--------

The Protocol uses TCP. It consists of *commands* and *events*, where *command* is something that is received by user module (server), and *event* is a state notification to the client. There can be multiple *events* or *commands* in one TCP-Package.

-  .. rubric:: Command: set GPIO output
      :name: command-set-gpio-output

   ::

        O<id><state>

   Where:

   -  *id* is two digit pin hex number
   -  *state* is 0 for low, 1 for high

   Examples:

   -  Set GPIO 13 low

      ::

           O0D0

   -  Set GPIO 8 high

      ::

           O081

-  .. rubric:: Event: GPIO input state
      :name: event-gpio-input-state

   ::

        I<id><state>

   Where:

   -  *id* is two digit pin hex number
   -  *state* is 0 for low, 1 for high

   Examples:

   -  GPIO input 2 set to low

      ::

           I020

   -  GPIO input 11 set to high

      ::

           I0B1

   -  GPIO output 1 and 2 are set to high

      ::

           O011O021

Usage
-----

Since linux 4.8 the GPIO sysfs interface is deprecated. User space
should use the character device instead. Current de-facto standard of
using new GPIO system is libgpiod library and its tools:

-  gpiodetect
-  gpioinfo
-  gpioget
-  gpioset
-  gpiofind
-  gpiomon

