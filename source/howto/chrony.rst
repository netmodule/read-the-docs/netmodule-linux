.. _chronydoc:

======
Chrony
======

Chrony is a versatile implementation of the Network Time Protocol (NTP).
It can synchronise the system clock with NTP servers, reference clocks (e.g. GPS receiver),
and manual input using wristwatch and keyboard. It can also operate as an NTPv4 (RFC 5905)
server and peer to provide a time service to other computers in the network.

`Chrony Website`_

.. _`Chrony Website`: https://chrony.tuxfamily.org/

Ensure that no other time daemon is running like ntp or ntimed to avoid conflicts.

Usage
-----

Two programs are included in chrony, chronyd is a daemon that can be started
at boot time and chronyc is a command-line interface program which can be used
to monitor chronyd’s performance and to change various operating parameters while it is running.

If chrony is already configured, time sources can be watched with:

::

    chronyc sources


Chrony Configuration File
~~~~~~~~~~~~~~~~~~~~~~~~~

Chrony daemon configuration file is located in directory ``/etc/chrony.conf``


NTP Servers
~~~~~~~~~~~

NTP time servers can be defined in configuration files. It is recommended to select
time servers which are physicaly close to the device. A pool of time servers can be found
at the `NTP Pool Project`_.

.. _`NTP Pool Project`: https://www.ntppool.org/

Example configuration:

::

    server 0.pool.ntp.org iburst
    server 1.pool.ntp.org iburst
    server 2.pool.ntp.org iburst



GNSS
~~~~

Chrony is able to use GNSS signal and their delivered time.
If GPSD is used as GNSS daemon, then chrony can access GPSD shared memory
to get time data.

Example configuration with GPSD shared memory access:
::

    refclock SHM 0 poll 1 refid GPS offset 0.0 delay 2 filter 16


RTC
~~~

Chrony can handle hardware real time clock (rtc), measure time drift and correct it automatically.
Define rtc device in configuration file by adding device path.

::

    rtcdevice /dev/rtc

More configuration option
-------------------------

Chrony is a powerfull tool and provides much more functionality than described here.
For further information see `Chrony Documentation`_

.. _`Chrony Documentation`: https://chrony.tuxfamily.org/documentation.html

