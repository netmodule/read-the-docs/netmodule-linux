******
(e)MMC
******

Health Status
=============
You can read your eMMC health status by using the ``mmc`` command and reading 
the EXT_CSD (extended card specific data) section. The life time estimation 
can be read for SLC and MLC areas as well as pre EOL status.

.. code-block:: bash

    ~# mmc extcsd read /dev/mmcblk1 | grep LIFE
    eMMC Life Time Estimation A [EXT_CSD_DEVICE_LIFE_TIME_EST_TYP_A]: 0x01
    eMMC Life Time Estimation B [EXT_CSD_DEVICE_LIFE_TIME_EST_TYP_B]: 0x01

    ~# mmc extcsd read /dev/mmcblk1 | grep EOL
    eMMC Pre EOL information [EXT_CSD_PRE_EOL_INFO]: 0x01


The life time values are provided in 10% steps:

* 0x01 = 0-10% device life time used
* 0x02 = 10-20% device life time used
* ...

whereas life time estimation type A is for SLC eraseblocks and type B is for MLC 
eraseblocks.

The EOL information is an overall status for reserved blocks, given as follows:

* 0x00 = Not defined
* 0x01 = Normal: consumed less than 80% of the reserved blocks
* 0x02 = Warning: consumed 80% of the reserved blocks
* 0x03 = Urgent: consumed 90% of the reserved blocks


Write Reliability
=================
Similar to the eMMC health status, you can check and set the write reliablity 
using the ``mmc`` command. The information reside also the EXT_CSD (extended 
card specific data) section. There are two parameters which are needed for the 
write reliability:

* ``WR_REL_PARAM``

    - the LSB [#thelsb]_ defines if ``WR_REL_SET`` is writable

        + LSB = 1: writable
        + LSB = 0: read-only

* ``WR_REL_SET``

    - shows the setup of reliable writing for each partition, whereas 
      partition 1 starts at the LSB [#thelsb]_

        + ``WR_REL_SET = ext_csd[EXT_CSD_WR_REL_SET] | (1<<partition);``


Check Write Reliability Setup
-----------------------------
Check the write reliability setup by reading EXT_CSD from your device and 
filtering the two parameters:

.. code-block:: bash

    ~# mmc extcsd read /dev/mmcblk0 | grep WR_REL_PARAM
    Write reliability parameter register [WR_REL_PARAM]: 0x05

    ~# mmc extcsd read /dev/mmcblk0 | grep WR_REL_SET
    Write reliability setting register [WR_REL_SET]: 0x1f


Value Explanation:

* ``WR_REL_PARAM = 0x05``

    - The LSB in ``WR_REL_PARAM`` is set, which means that ``WR_REL_SET`` is 
      writable. I.e. read-only would have shown ``WR_REL_PARAM: 0x04``

* ``WR_REL_SET = 0x1f``

    - The LSB in ``WR_REL_SET`` is set, which means that reliable write is 
      enable for partition 1. I.e. disabled would have shown ``WR_REL_SET = 0x1e``


Enable Write Reliability
------------------------
Enabling reliable writing can be done with the mmc command 
``write_reliability set`` as seen in the help:

.. code-block:: bash

    ~# mmc --help
    ...
    mmc write_reliability set <-y|-n|-c> <partition> <device>
            Enable write reliability per partition for the <device>.
            Dry-run only unless -y or -c is passed.
            Use -c if more partitioning settings are still to come.
            NOTE!  This is a one-time programmable (unreversible) change.
    ...

whereas a dry run can be made by providing the parameter -n.

.. note::
    Setting write reliability can only be done once (unreversible)!



Storage Information
===================
The information described in the upper sections are as well collected by a 
small application called `storage-info`. This application reports the values 
in a human readable format to the file ``/run/storage/storage0.config``:

.. code-block:: bash

    ~# cat /run/storage/storage0.config 
    Chip Name:                DG4008
    HW Revision:              0x0
    Revision:                 0x8
    FW Version:               39102417
    Serial Number:            0x65b0470c
    Reliable Writable:        1
    Reliable Write Config:    0x1f
    Pre-EOL Information:      1 = Normal
    Lifetime used (SLC):      10%
    Lifetime used (MLC):      10%
    Disk space free:          492.7MB



.. rubric:: Footnotes

.. [#thelsb] LSB = Least Significant Bit, the bit number 0 (bit with index 0)
