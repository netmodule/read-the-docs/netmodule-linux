Package Management using DNF
============================

For full documentation visit
dnf.readthedocs.io <https://dnf.readthedocs.io/en/latest/index.html>`_.

Setup
-----

Add the netmodule package repository

::

       mkdir /etc/yum.repos.d
       touch /etc/yum.repos.d/oe-packages.repo

-  Add following lines to ``/etc/yum.repos.d/oe-packages.repo``

::

       [oe-packages]
       baseurl=https://nmrepo.netmodule.com/chbe/rpm/
       gpgcheck=False



Commands
--------

-  ``dnf search <pkg-name>`` - Search package
-  ``dnf install <pkg-name>`` - Install package
-  ``dnf remove <pkg-name>`` - Remove package

OSTree compatibility
--------------------

To use dnf with ostree you have to do the following steps:

1. mount an overlay-fs
   You might use the
   `auto-partition-script <auto-partition.rst#Recommended%20settings>`_
   with the recommended settings.

2. Add symbolic link to /lib/rpm

::

  ln -s /usr/lib/rpm /var/lib/rpm

Hint: If this fails, remove the ``/var/lib/rpm`` directory and rerun this step.
