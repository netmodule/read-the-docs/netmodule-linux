IMU
===

ST provides LSM6DSx driver which provides two different ways of reading out IMU data.

.. note::
    Changes betweeen kernel 4.19 and 5.10:

    * The gyroscope and accelerometer have switched numbering:

      * gyroscope is now at /sys/bus/iio/devices/iio\:device0
      * accelerometer is now at /sys/bus/iio/devices/iio\:device1

    * There is only one scale value for all axes instead of one per axis

Polling mode
------------

Polling mode is the simplest IMU driver configuration, where data is read out on request.
This mode does not support hardware timestamping.

Accelerometer
^^^^^^^^^^^^^

Get raw value of z-axis

::

    cat /sys/bus/iio/devices/iio\:device1/in_accel_z_raw

Get scale value

::

    cat /sys/bus/iio/devices/iio\:device1/in_accel_scale

Multiply the two to get the acceleration in m/s2. For z-axis this should
be around 9.81m/s2.

**Example:**

::

    cat /sys/bus/iio/devices/iio\:device1/in_accel_z_raw
    # 16499

    cat /sys/bus/iio/devices/iio\:device1/in_accel_scale
    # 0.000598

    # --> 9.8664

Gyro
^^^^

Get raw value of x-axis

::

    cat /sys/bus/iio/devices/iio\:device0/in_anglvel_x_raw


Buffered mode
-------------

Buffered mode enables full driver functionality, but requires hardware interrupts to be processed by the driver, which consumes more CPU load.
This mode supports hardware timestamping.

Configuration
^^^^^^^^^^^^^

Buffered mode requires configuration. Following is a typical configuration to receive all the data IMU provides, including hardware timestamps:

Accelerometer
"""""""""""""

::

    # Disable buffered mode

    echo 0 > /sys/bus/iio/devices/iio\:device1/buffer/enable

    # Subscribe to data elements

    echo 1 > /sys/bus/iio/devices/iio\:device1/scan_elements/in_accel_x_en
    echo 1 > /sys/bus/iio/devices/iio\:device1/scan_elements/in_accel_y_en
    echo 1 > /sys/bus/iio/devices/iio\:device1/scan_elements/in_accel_z_en
    echo 1 > /sys/bus/iio/devices/iio\:device1/scan_elements/in_timestamp_en

    # Set sampling frequency

    echo 13 > /sys/bus/iio/devices/iio\:device1/sampling_frequency

    # Set buffer length

    echo 2 > /sys/bus/iio/devices/iio\:device1/buffer/length

    # Enable buffered mode

    echo 1 > /sys/bus/iio/devices/iio\:device1/buffer/enable

Gyro
""""

::

    # Disable buffered mode

    echo 0 > /sys/bus/iio/devices/iio\:device0/buffer/enable

    # Subscribe to data elements

    echo 1 > /sys/bus/iio/devices/iio\:device0/scan_elements/in_anglvel_x_en
    echo 1 > /sys/bus/iio/devices/iio\:device0/scan_elements/in_anglvel_y_en
    echo 1 > /sys/bus/iio/devices/iio\:device0/scan_elements/in_anglvel_z_en
    echo 1 > /sys/bus/iio/devices/iio\:device0/scan_elements/in_timestamp_en

    # Set sampling frequency

    echo 13 > /sys/bus/iio/devices/iio\:device0/sampling_frequency

    # Set buffer length

    echo 2 > /sys/bus/iio/devices/iio\:device0/buffer/length

    # Enable buffered mode

    echo 1 > /sys/bus/iio/devices/iio\:device0/buffer/enable

Data
^^^^

Reading out IMU data in buffered mode is done in a different way comparing to polling mode.
In polling mode data is provided via driver SysFS nodes, while in buffered mode data is read out via dedicated char devices.

Accelerometer
"""""""""""""

::

    hexdump /dev/accel0

Gyro
""""

::

    hexdump /dev/gyro0

Reconfiguration
^^^^^^^^^^^^^^^

Buffered mode does not allow reconfiguration on the fly. However, it is allowed to temporarily disable buffered mode while IMU char device is opened, which gives a possibility to reconfigure IMU driver.
For example, if some application is used to read the data from accelerometer (see example above), it is possible to change its sampling frequency without closing the application:

::

    echo 0 > /sys/bus/iio/devices/iio:device1/buffer/enable
    echo 13 > /sys/bus/iio/devices/iio:device1/sampling_frequency
    echo 1 > /sys/bus/iio/devices/iio:device1/buffer/enable
