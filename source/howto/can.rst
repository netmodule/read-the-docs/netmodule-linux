CAN
===

CAN Interfaces
--------------

Linux provides CAN driver for physical available CAN controller and for
virtual created CAN adapter so called vcan.

Physical CAN Interface
~~~~~~~~~~~~~~~~~~~~~~

Physical can interfaces depends on hardware and driver support.
To check if physical can interfaces are available do:

::

    ifconfig -a | grep can

Output should be similar to following:

::

    can0      Link encap:UNSPEC  HWaddr 00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00
    can1      Link encap:UNSPEC  HWaddr 00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00

To initialize an interface, do the following::

   ip link set can0 type can bitrate 125000
   ip link set up can0

It is then possible to send and receive data with::

   cansend can0 1FFFFFFF#112233445D556677
   candump can0

Virtual CAN Interface - vcan
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To bring up virtual can interface the kernel module vcan is required.
Load vcan module:

::

    modprobe vcan

And controls whether the module is loaded successfully:

::

    lsmod | grep vcan

Output should be similar to following:

::

    vcan                   16384  0

Now a virtual can interface vcan0 can be created:

::

    ip link add dev vcan0 type vcan
    ip link set vcan0 mtu 16
    ip link set up vcan0

To bring up CAN FD interface mtu size must increased to 72:

::

    ip link add dev vcan0 type vcan
    ip link set vcan0 mtu 72
    ip link set up vcan0

And again control new created virtual can interface:

::

    ifconfig vcan0

Output should be similar to following:

::

    vcan0     Link encap:UNSPEC  HWaddr 00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00
              UP RUNNING NOARP  MTU:16  Metric:1
              RX packets:0 errors:0 dropped:0 overruns:0 frame:0
              TX packets:0 errors:0 dropped:0 overruns:0 carrier:0
              collisions:0 txqueuelen:1000
              RX bytes:0 (0.0 B)  TX bytes:0 (0.0 B)

From this point the virtual can interface vcan0 can be used e.g. for
SocketCAN.

SocketCAN
---------

SocketCAN is used to tunnel CAN data over TCP/IP (e.g. ethernet). 
For linux based system, Cannelloni and socketcand are examples for 
user space tool which use SocketCAN.


Bind virtual can adapter vcan0 to any counterpart:

::

   cannelloni -I vcan0 -R <remote ip> -r <remote port> -l <local port>

For non blocking console append a ``&`` in the command above.

Cannelloni Example
~~~~~~~~~~~~~~~~~~

.. image:: ../images/cannelloni-example.png
   :alt: cannelloni example

In this example you see Machine 1 with a physical CAN-Interface (can0) and Machine 2 with a virtual CAN-Interface (vcan0).
With cannelloni its now possible to link the two CAN-Interfaces together via the TCP/IP stack (UDP and SCTP).
This makes it possible to read and write the physical CAN from Machine 2 as if it were directly connected to Machine 2. This is useful if you want to process the CAN-data on an external Machine.

Example use of SocketCAN between two machines.

+----------------------------+--------------------+--------------------+
| Config                     | MACHINE 1          | MACHINE 2          |
+============================+====================+====================+
| IP                         | 192.168.0.2        | 192.168.0.3        |
+----------------------------+--------------------+--------------------+
| Local cannelloni port      | 2000               | 2000               |
+----------------------------+--------------------+--------------------+

**MACHINE 1**

::

    cannelloni -I can0 -R 192.168.0.3 -r 2000 -l 2000

**MACHINE 2**

::

    cannelloni -I vcan0 -R 192.168.0.2 -r 2000 -l 2000

Now you can test the functionallity with the following commands.

**MACHINE 1**

::

    candump can0

**MACHINE 2**

::

    cansend vcan0 1FFFFFFF#112233445D556677

You should now see the data on can0.

External Physical CAN Interface
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Ensure that any can participant is on can bus. For communication verification a
can PC interface is recommendend. Check also that physical bus is proper
terminated with 120 Ohm impedance.

can-utils
---------

can-utils provides severals tools to e.g. interact and monitor general
can interfaces.

To send can frames to vcan0 command cansend can be used:

::

    cansend <device> <can_frame>
    # example
    cansend vcan0 5A2#11.2233.445D556677.66

To dump can frames on a can interface use command candump:

::

    candump <device>
    # example
    candump vcan0

Resources
~~~~~~~~~

Cannelloni: `https://github.com/mguentner/cannelloni <https://github.com/mguentner/cannelloni>`_
