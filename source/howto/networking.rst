NetModule Linux Networking
==========================

NetworkManager
--------------

nmcli
~~~~~

-  ``nmcli c edit ethernet`` - edit the ethernet connection interface.
-  ``nmcli c modify ethernet ipv4.method auto`` - oneline edit, useful for scripts
