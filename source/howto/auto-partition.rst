Automatic Partitioning
======================

When you flashed a ".wic"-file your system will only have one partition
with almost no space. To make the filesystem usable, we provide an
auto-partitioning tool called ``nmhw-auto-part``.

Recommended settings
--------------------

1. Run the script in interactive mode.

::

  nmhw-auto-part -i

2. Press enter without entering values on all the steps.

Usage
-----

::

  nmhw-auto-part.sh [OPTION] 'devicename' 'size' 'mode' 'size'

Arguments
~~~~~~~~~

'devicename'
  device to use: e.g. /dev/sdb /dev/mmcblk1

'size'
  size of the first partition in MB (set 0 to keep current size)

'mode'
  'data' or 'overlay'; type of partition to append

'size'
  size of the appended partition in MB (set to 0 to fill)

Options
~~~~~~~

::

  -i interactive mode
  -h help

You can either pass the four arguments 'devicename', 'size', 'mode' and
'size' or one option.

Examples
--------

::

  nmhw-auto-part /dev/mmcblk1 1024 overlay 0

The same as running the recommended settings.

How to undo the partitioning
----------------------------

1. Delete Partition information

::

  rm -r /etc/nmhw-auto-part
  reboot
  parted /dev/mmcblk1 rm 2

4. Rerun the autopartition script.

Note: The first partition will keep its size.

Files in /etc
~~~~~~~~~~~~~

``/etc/nmhw-auto-part/overlay``
  This is an empty file. It indicates to the init script that it needs to mount an overlay.

``/etc/nmhw-auto-part/data-partition``
  This file contains the path of
  the device, which serves as the data partition. It indicates to the
  init script that it needs to mount the data partition.
