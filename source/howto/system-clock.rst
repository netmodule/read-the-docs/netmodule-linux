========================
System Clock / Date Time
========================

There might be several time giving sources like GNSS, NTP, etc that can be used to synchronize the system clock and other clock dependent parts.


Synchronization
---------------
Chrony is able to perform such clock synchronizations. For further information please have a look at :ref:`chronydoc`. Chrony is also able to synchronize the HW clock (RTC) automatically if it drifts away.

Another tool to synchronize/handle/update the HW clock (RTC) is ``timedatectl`` which competes against chrony. **This means be cautious when synchronizing/updating the HW clock manually**


HW Clock Synchronization/Update
-------------------------------
As described in section above the RTC can be updated using different tools. The following sections show how this can be performed.

timedatectl
~~~~~~~~~~~
**NOTE:** Using ``timedatectl`` you are able to update the HW clock manually.

If no time source is present no update takes place automatically. Then a manual update is possible with:
::
  
  timedatectl set-local-rtc n

Please read also the `timedatectl man page <https://man7.org/linux/man-pages/man1/timedatectl.1.html>`_ when using ``timedatectl``.
