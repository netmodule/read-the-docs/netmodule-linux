LEDs
====

NMHW21 provides four LEDs that can be controlled by user application or
via command line.

The LEDs are described as follows:

- Indicator onboard (ind)
- Status onboard (status)
- Indicator user-interface (ind ui)
- Status user-interface (status ui)

Standard LED behavior
---------------------

On startup (during bootloader/system-boot) the LEDs follow a specific pattern.
During the startup the LEDs on the user-interface and the onboard LEDs are synced.

| 'ind' = 'ind ui'
| 'status' = 'status ui'

- start of the bootloader (hard coded into u-boot)

| ind - red
| status - red

- start of kernel (defined in the device tree)

| ind - off
| status - orange

Hard reset
^^^^^^^^^^
On a hard reset the onboard status LED starts orange.

Mainboard Indicator
-------------------

**Green**

::

    $ echo 1 > /sys/class/leds/ind\:green/brightness
    $ echo 0 > /sys/class/leds/ind\:red/brightness

**Orange**

::

    $ echo 1 > /sys/class/leds/ind\:green/brightness
    $ echo 1 > /sys/class/leds/ind\:red/brightness

**Red**

::

    $ echo 0 > /sys/class/leds/ind\:green/brightness
    $ echo 1 > /sys/class/leds/ind\:red/brightness

**Off**

::

    $ echo 0 > /sys/class/leds/ind\:green/brightness
    $ echo 0 > /sys/class/leds/ind\:red/brightness

Mainboard Status
----------------

**Green**

::

    $ echo 1 > /sys/class/leds/status\:green/brightness
    $ echo 0 > /sys/class/leds/status\:red/brightness

**Orange**

::

    $ echo 1 > /sys/class/leds/status\:green/brightness
    $ echo 1 > /sys/class/leds/status\:red/brightness

**Red**

::

    $ echo 0 > /sys/class/leds/status\:green/brightness
    $ echo 1 > /sys/class/leds/status\:red/brightness

**Off**

::

    $ echo 0 > /sys/class/leds/status\:green/brightness
    $ echo 0 > /sys/class/leds/status\:red/brightness

User Interface Indicator
------------------------

**Green**

::

    $ echo 1 > /sys/class/leds/ui\:ind\:green/brightness
    $ echo 0 > /sys/class/leds/ui\:ind\:red/brightness

**Orange**

::

    $ echo 1 > /sys/class/leds/ui\:ind\:green/brightness
    $ echo 1 > /sys/class/leds/ui\:ind\:red/brightness

**Red**

::

    # echo 0 > /sys/class/leds/ui\:ind\:green/brightness
    $ echo 1 > /sys/class/leds/ui\:ind\:red/brightness

**Off**

::

    # echo 0 > /sys/class/leds/ui\:ind\:green/brightness
    $ echo 0 > /sys/class/leds/ui\:ind\:red/brightness

User Interface Status
---------------------

**Green**

::

    $ echo 1 > /sys/class/leds/ui\:status\:green/brightness
    $ echo 0 > /sys/class/leds/ui\:status\:red/brightness

**Orange**

::

    $ echo 1 > /sys/class/leds/ui\:status\:green/brightness
    $ echo 1 > /sys/class/leds/ui\:status\:red/brightness

**Red**

::

    $ echo 0 > /sys/class/leds/ui\:status\:green/brightness
    $ echo 1 > /sys/class/leds/ui\:status\:red/brightness

**Off**

::

    $ echo 0 > /sys/class/leds/ui\:status\:green/brightness
    $ echo 0 > /sys/class/leds/ui\:status\:red/brightness
