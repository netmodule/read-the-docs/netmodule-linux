====
GNSS
====

The Global Navigation Satellite System can be accessed using different tools: gpsd provides a useful toolset and the gnss-mgr provides configuration possibilities.


gpsd
----
`gpsd <https://gpsd.gitlab.io/gpsd/index.html>`_ is used as the interface between the GNSS receiver and other location aware applications.
Multiple applications can access the GNSS receiver via TCP connections on port 2947 at the same time, solving the problem of multiple applications requiring access to the same tty interface.
gpsd includes several tools to interface to the GNSS receiver, like cpgs, gpsctl, gpscat and ubxtool.

Connecting ubxtool
------------------
The ubxtool might not establish a stable connection to the modem (interruptions, connection issues, ...) depending on how the modem is connected internally. The following command helps to get a stable connection with ubxtool:

::

  ser2net -C "2947:raw:0:/dev/gnss0: 115200 8DATABITS NONE 1STOPBIT NOBREAK"



NEO-M8L
-------
The `NEO-M8L module <https://www.u-blox.com/sites/default/files/NEO-M8L_ProductSummary_%28UBX-16000760%29.pdf>`_ is a GNSS receiver by u-blox with the following key features:

- Automotive Grade
- GPS / QZSS, GLONASS, Galileo and BeiDou
- Dead Reckoning using built in IMU

NEO-M9V
-------
The `NEO-M9V module <https://www.u-blox.com/en/product/neo-m9v-module>`_ is the replacement of the NEO-M8 and provided similar features.


gnss-mgr
--------
``gnss-mgr`` provides GNSS receiver configuration possibilities and replaces the obsolete ``gnss-config``, ``gnss-save-on-shutdown`` and ``neom8tool``.
This tool operates directly on the serial interface *before* gpsd is staring up and comes with different functionalities like:

- initialization (runs on each boot-up)
   - configuring the communciation bitrate of the receiver if necessary
   - configuring the used NMEA protocol version if necessary
   - clearing latest receiver state (save on shutdown)
- save on shutdown (SoS) features like persisting/clearing/verifying receiver state to/from internal storage
- configuring the receiver using a configuration file (in ini file format, see below)
- control functions
   - persist the actual configuration into non-volatile storage
   - cold-start the receiver
   - resetting the receiver's configuration to default

The gnss-mgr is set up as systemd service (``gnss-mgr.service``).

On each boot-up the ``gnss-mgr`` checks and if necessary configures the bitrate and the NMEA protocol version. The ``gnss-mgr`` configures the recevier with volatile parameters, i.e. the configuration is not persisted and when the power is cut, the receiver looses your configuration (the receiver then re-applies its internally stored setup at the next power-up). Additionally after successfully starting up the gnss services, some basic information about the GNSS receiver can be found at ``/run/gnss/gnss0.conf``.

::
  
  root@am335x-nmhw21:~# cat /run/gnss/gnss0.config
  Vendor:                             ublox
  Model:                              NEO-M8L-0
  Firmware:                           ADR 4.21 (Deprecated)
  ubx-Protocol:                       19.20
  Supported Satellite Systems:        GPS;GLO;GAL;BDS
  Supported Augmentation Services:    SBAS;IMES;QZSS
  SW Version:                         EXT CORE 3.01 (1ec93f)
  HW Version:                         00080000


Capabilities
~~~~~~~~~~~~
The command ``gnss-mgr --help`` shows the entire capabilities of the ``gnss-mgr``:
::
  
  root@am335x-nmhw21:~# gnss-mgr --help
  usage: gnss-mgr [-h] [-V] [-v] [-q] device {init,sos,config,control} ...
  
  Manages GNSS modem
  
  positional arguments:
    device                local serial device to which GNSS modem is connected
                          (e.g. /dev/gnss0)
  
  optional arguments:
    -h, --help            show this help message and exit
    -V, --version         show program's version number and exit
    -v, --verbose         be verbose, show debug output
    -q, --quiet           be quiet, only show warnings and errors
  
  command:
    {init,sos,config,control}
                          select command
      init                sets up GNSS modem
      sos                 save on shutdown operations
      config              configures GNSS modem
      control             performs GNSS modem control function


Each command (init, sos, config and control) provides a sub-help:
::
  
  root@am335x-nmhw21:~# gnss-mgr /dev/gnss0 init --help
  usage: gnss-mgr device init [-h] [-f RUNFILE]
  
  optional arguments:
    -h, --help            show this help message and exit
    -f RUNFILE, --file RUNFILE
                          path to run file
  
  ----------------------------
  
  root@am335x-nmhw21:~# gnss-mgr /dev/gnss0 sos --help
  usage: gnss-mgr device sos [-h] {save,clear}
  
  positional arguments:
    {save,clear}  selects sos operation to perform
  
  optional arguments:
    -h, --help    show this help message and exit
  
  ----------------------------
  
  root@am335x-nmhw21:~# gnss-mgr /dev/gnss0 config --help
  usage: gnss-mgr device config [-h] [-f CONFIGFILE]
  
  optional arguments:
    -h, --help            show this help message and exit
    -f CONFIGFILE, --file CONFIGFILE
                          path to config file
  
  ----------------------------
  
  root@am335x-nmhw21:~# gnss-mgr /dev/gnss0 control --help
  usage: gnss-mgr device control [-h] {cold-start,persist,factory-reset}
  
  positional arguments:
    {cold-start,persist,factory-reset}
                          selects action to perform
  
  optional arguments:
    -h, --help            show this help message and exit



Configuring the GNSS Receiver
-----------------------------
The ``gnss-mgr`` configures the GNSS receiver by using configuration files in ini format. Writing the configuration is managed by feeding the appropriate file to the gnss-mgr.


.. warning:: A misconfigured GNSS with Dead Reckoning (DR) generates worse results as a GNSS receiver only!


.. note:: When running the GNSS receiver at 9600 baud some UBX packets might be lost which results in overall worse GNSS performance. Therefore it is strongly recommended to let the receiver operate with 115200 baudto. Additionally it is recommended to configure the GNSS receiver persistent (see example *Persistent configuration of the GNSS receiver* below.


Configuration files
~~~~~~~~~~~~~~~~~~~
Use the configuration file ``/etc/gnss/gnss0.conf`` as template to properly configure the GNSS receiver.

.. warning:: The IMU angles (yaw, pitch, roll) are different between NEO-M9 and M8

The following example of gnss0 config file shows the configuration capabilities of the ``gnss-mgr``. The comments show how to setup each parameter:
::
  
  root@am335x-nmhw21:~# cat /etc/gnss/gnss0.conf      
  #
  # This file is part of gnss-mgr service
  # To make changes, edit the values in this file and reload
  # gnss-mgr service.
  #
  # Any empty value will use the default or persistent configuration
  # of the receiver.
  
  [default]
  # Indicates the version of this config file, it should not be modified.
  # If unsure of its value, sample config file can always be found in
  # /usr/etc/gnss/
  #
  # Change history
  # version 3: Added field navigation.dead-reckoning
  # version 4: Added "time" section
  version=4

  # Select measurement and navigation output rate
  # Allowed values : 1 - 10  [Hz]
  update-rate=
  #update-rate=1
  
  
  #
  # Navigation settings
  #
  [navigation]
  # Enables or disables dead reckoning
  # Supported values:
  #   enable, disable
  dead-reckoning=
  #dead-reckoning=disable
  
  # Selects dynamic mode
  # Supported values:
  #   stationary, vehicle
  mode=
  #mode=vehicle
  
  #
  # Selects GNSS systems
  # Allowed values:
  #   GPS;GLONASS;SBAS
  #   GPS;Galileo;Beidou;SBAS
  systems=
  #systems=GPS;GLONASS;SBAS
  #systems=GPS;Galileo;Beidou;SBAS
  
  
  #
  # Installation settings
  # For details on this section, see the relevant documentation
  #
  [installation]
  
  #
  # IMU orientation in degrees [°]
  #
  # ! Be careful, the IMU orientation may be different between
  #   NEO-M8 and NEO-M9
  #
  # ! Also configuring these angles will disable auto-alignment
  #
  #   yaw: value in degrees (0 to  360)
  #   pitch: value in degrees (-90 to  90)
  #   roll: value in degrees (-180 to 180)
  yaw=
  pitch=
  roll=
  
  # Lever arm lengths in meters [m]
  # Format x;y;z
  # Example:
  #   vrp2antenna=1.0;1.5;0.3
  vrp2antenna=
  vrp2imu=


  #
  # Configurations related to time
  # For details on this section, see the relevant documentation
  #
  [time]

  #
  # Enables or disables the time pulse (PPS) line
  # If it is not set, the default or already configured
  # state of the receiver will be used.
  #
  # NB: This feature may not be available on all systems
  # Handling of the PPS line must also be done.
  #
  timepulse=
  #timepulse=enable
  #timepulse=disable


  #
  # Sets the frequency [Hz] of the time pulse (PPS) line
  #
  # If not set the default or already configured value is used
  #
  timepulse-frequency=
  #timepulse-frequency=1
  #timepulse-frequency=20



Lever Arm Lengths
~~~~~~~~~~~~~~~~~
The following figure shows an installation example and how to configure the receiver's lever arm lengths.

.. figure:: lever_arm_length.svg
    :width: 100%
    :alt: Lever Arm Lengths Example
    :figclass: align-center
    
    Installation and positioning

LeverConfiguration with values
  - vrp2antenna=1.6;0;1.7
  - vrp2imu=2.1;0.6;0.8


Timepulse configuration
~~~~~~~~~~~~~~~~~~~~~~~

The timepulse can be enable or disabled and configured in the [time] section.
This feature sends a pulse every 1/X seconds. Note that the PPS line must be
handled outside of gnss-mgr: depending on the system, it may be:

 * not connected
 * connected directly to the CPU and in this case handled by the "time"
   software
 * connected to an external component


Examples
~~~~~~~~
Configuring the GNSS receiver:
::
  
  gnss-mgr /dev/gnss0 config -f /etc/gnss/gnss0.conf


Persisting the configuration of the GNSS receiver:
::
  
  gnss-mgr /dev/gnss0 control persist


Resetting the receiver's configuration to default:
::
  
  gnss-mgr /dev/gnss0 control factory-reset


Troubleshooting
~~~~~~~~~~~~~~~~
If the receiver is no longer accessible, the issue most seen is that baud rates of serial interface (ttyS3) and receiver are not matching.


Save on Shutdown
----------------

The u-blox GNSS receivers can be instructed to save their current state to the internal non-volatile memory and restore it after a power cycle.

Note that the receiver state and the receiver configuration must be distinguished:
Saving the state before the system is shut-down can help your GNSS receiver to get a faster fix after booting.
Save on shutdown does however not save any configuration done by the user. The receiver configuration needs to be saved manually to the receiver's internal non-volatile memory (only once), see Example *Persiting the configuration of the GNSS receiver*.

The ``gnss-mgr`` service instructs the GNSS receiver to save its state whenever your linux system receives a shutdown or reboot instruction.
Upon reboot the same service logs if the GNSS receiver state has been successfully restored.


Examples
~~~~~~~~
Storing the state to the GNSS receiver's internal storage:
::
  
  gnss-mgr /dev/gnss0 sos save

Clearing the state in the GNSS receiver's internal storage:
::
  
  gnss-mgr /dev/gnss0 sos clear



Firmware update (specific to u-blox NEO-M8L)
--------------------------------------------
The firmware of the module u-blox NEO-M8L can be upgraded as explained in :ref:`fw-update`.:


Testing
-------
To test the GNSS function connect an active GNSS antenna to X3300
"GNSS".

Run "cgps" tool

Your output should look like this. Typically it takes 3..20 seconds for
a fix.

::
  
    ┌───────────────────────────────────────────┐┌─────────────────────────────────┐
    │    Time:       2018-07-05T06:49:00.000Z   ││PRN:   Elev:  Azim:  SNR:  Used: │
    │    Latitude:    47.31890666 N             ││   6    18    082    28      Y   │
    │    Longitude:    7.97375949 E             ││  17    15    039    30      Y   │
    │    Altitude:   1641.076 ft                ││  19    35    052    27      Y   │
    │    Speed:      0.14 mph                   ││  32    32    305    16      Y   │
    │    Heading:    0.0 deg (true)             ││  66    27    311    23      Y   │
    │    Climb:      0.00 ft/min                ││  74    43    076    28      Y   │
    │    Status:     3D FIX (3 secs)            ││  84    46    063    21      Y   │
    │    Longitude Err:   +/- 33 ft             ││                                 │
    │    Latitude Err:    +/- 111 ft            ││                                 │
    │    Altitude Err:    +/- 200 ft            ││                                 │
    │    Course Err:      n/a                   ││                                 │
    │    Speed Err:       +/- 152 mph           ││                                 │
    │    Time offset:     7176.328              ││                                 │
    │    Grid Square:     JN37xh                ││                                 │
    └───────────────────────────────────────────┘└─────────────────────────────────┘

Access gps interface
--------------------
If direct interface access is required, the gps device is
available under ``/dev/gnss0``.
Be aware that ``gnss0`` is just a symlink to the real device managed by udev.
If you need to know the real device, follow the symlink with:

Example output:

.. code-block:: console

    root@am335x-nmhw21:~# ls -l /dev/gnss0
    lrwxrwxrwx 1 root root 5 Jul  2 07:06 /dev/gnss0 -> ttyS3
