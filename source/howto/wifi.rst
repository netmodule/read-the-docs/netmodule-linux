Wi-Fi
=====

Overview
--------

-  Used WIFI (and BT) Chip: WL1837MOD
-  linux-firmware repository:
   ``git://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git``

-  Commit id: 4c0bf113a55975d702673e57c5542f150807ad66
-  Which is basically: wl18xx: update firmware file 8.9.0.0.76

-  NetworkManager (nmcli) connection configuration files are stored
   under ``/etc/NetworkManager/system-connections``

802-11 Standard
~~~~~~~~~~~~~~~

-  a: 5 GHz-Band, up to 54 Mbits/s
-  b: 2.4 GHz-Band, up to 11 Mbits/s
-  g: 2.4 GHz-Band, up to 54 Mbits/s
-  n: 2.4 & 5 GHz-Band, up to 600 Mbits/s
-  ac: 5 GHz-Band, up to 1.3 Gbits/s

Client Mode
-----------

Scan and connect to existing access point (AP)

::

    $ nmcli d wifi list
    $ nmcli d wifi connect <MYWLAN> password <my_password>

Access Point Mode
-----------------

*Note: Set region code to get right channels!*

Create Access Point in 2.4 GHz Band
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Create hotspot with SSID Hostspot24, no encryption

::

    $ nmcli con add type wifi ifname wlan0 con-name Hostspot24 autoconnect yes ssid Hostspot24
    $ nmcli con modify Hostspot24 802-11-wireless.mode ap 802-11-wireless.band bg ipv4.method shared
    $ nmcli con up Hostspot24

Create hotspot with SSID SecSpot, wpa-psk encryption with password: 12345678

*Note: for wpa-psk encryption password has to be 8 characters or more.*
::

    $ nmcli con add type wifi ifname wlan0 con-name SecSpot autoconnect yes ssid SecSpot
    $ nmcli con modify SecSpot 802-11-wireless.mode ap 802-11-wireless.band bg ipv4.method shared
    $ nmcli con modify SecSpot wifi-sec.key-mgmt wpa-psk
    $ nmcli con modify SecSpot wifi-sec.psk "12345678"
    $ nmcli con up SecSpot

Create Access Point in 5 GHz Band
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Create hotspot with SSID Hostspot50, no encryption

::

    $ nmcli con add type wifi ifname wlan0 con-name Hostspot50 autoconnect yes ssid Hostspot50
    $ nmcli con modify Hostspot50 802-11-wireless.mode ap 802-11-wireless.band a ipv4.method shared
    $ nmcli con up Hostspot50

Create hotspot with SSID SecSpot5, wpa-psk encryption with password: 12345678

*Note: for wpa-psk encryption password has to be 8 characters or more.*

::

    $ nmcli con add type wifi ifname wlan0 con-name SecSpot5 autoconnect yes ssid SecSpot5
    $ nmcli con modify SecSpot5 802-11-wireless.mode ap 802-11-wireless.band a ipv4.method shared
    $ nmcli con modify SecSpot5 wifi-sec.key-mgmt wpa-psk
    $ nmcli con modify SecSpot5 wifi-sec.psk "12345678"
    $ nmcli con up SecSpot5
