===================
Data Volume Monitor
===================

Introduction
------------

We introduced a data volume monitor called `vnstat` in our develop image, see  `<https://humdi.net/vnstat/>`_ for more information about `vnstat`.


Configuration
-------------

`vnstat` can be configured with the config file `/etc/vnstat.conf`

Our configuration sets `wwan0` as default interface and runs the used database on a ramdisk (volatile). The responsible configuration values are the follows:

.. code-block:: shell-session

    DatabaseDir "/run/vnstat"
    Interface "wwan0"


The database is on path `/run/vnstat/vnstat.db`.

.. note::
   This volatile configuration gets lost at each reboot. If you want to persist the database then follow the guide in the next section.

   The default configuration synchronizes the database every 5 minutes, so be aware that there is a latency in the displayed numbers.



Persisting Data base
--------------------

The following lines show you about how you can persist the data volume over reboots:

1. Stop the service: `systemctl stop vnstat`
2. Adapt the configuration:

   - in `/etc/vnstat.conf` set the following setting: `DatabaseDir "/var/lib/vnstat"`
3. Start the service: `systemctl start vnstat`

The service runs then on the database residing on the emmc. It will create a new database if no one is existing or continuing with the database residing in `/var/lib/vnstat`.



How To use
----------

The help of vnstat is somehow self explaining and shows you how to use it:

.. code-block:: shell-session

    vnstat --help

    vnStat 2.6 by Teemu Toivola <tst at iki dot fi>

      -5,  --fiveminutes [limit]   show 5 minutes
      -h,  --hours [limit]         show hours
      -hg, --hoursgraph            show hours graph
      -d,  --days [limit]          show days
      -m,  --months [limit]        show months
      -y,  --years [limit]         show years
      -t,  --top [limit]           show top days

      -b, --begin <date>           set list begin date
      -e, --end <date>             set list end date

      --oneline [mode]             show simple parsable format
      --json [mode] [limit]        show database in json format
      --xml [mode] [limit]         show database in xml format

      -tr, --traffic [time]        calculate traffic
      -l,  --live [mode]           show transfer rate in real time
      -i,  --iface <interface>     select interface (default: wwan0)

    Use "--longhelp" or "man vnstat" for complete list of options.


**Example:** show only the wwan0 data volume:

.. code-block:: shell-session

    vnstat -i wwan0

    Database updated: 2021-03-24 10:55:01

    wwan0 since 2021-03-24

          rx:  84.78 KiB      tx:  84.83 KiB      total:  169.62 KiB

    monthly
                     rx      |     tx      |    total    |   avg. rate
     ------------------------+-------------+-------------+---------------
       2021-03     84.78 KiB |   84.83 KiB |  169.62 KiB |        0 bit/s
     ------------------------+-------------+-------------+---------------
     estimated       --      |     --      |     --      |

    daily
                     rx      |     tx      |    total    |   avg. rate
     ------------------------+-------------+-------------+---------------
         today     84.78 KiB |   84.83 KiB |  169.62 KiB |       35 bit/s
     ------------------------+-------------+-------------+---------------
     estimated       186 KiB |     186 KiB |     371 KiB |

