Ethernet
========

Overview
--------

NetModule OEM Linux Distribution comes with built-in support for a
number of Ethernet devices. This includes support for hardware solutions
as simple as single port standard Ethernet PHY and very complex hardware
architecture involving cascading Ethernet switches with external PHYs.

Supported scenarios
-------------------

Basic setup
~~~~~~~~~~~

Single network for internal (umnet0) and external (eth0, broadr0 and
broadr1) interfaces.

Multiple isolated interfaces
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Up to 4 isolated network connections thanks to SJA1105TEL VLAN tagging.
IP addresses assigned to VLAN aware interfaces on top of eth0. See
`example <#multiple-isolated-interfaces-example>`_.

Supported hardware
------------------

NMHW21
~~~~~~

-  TI Common Platform Ethernet Switch (CPSW)
-  NXP SJA1105TEL Five- Ports AVB & TSN Automotive Ethernet Switch
-  NXP TJA1100 100BASE-T1 PHY for Automotive Ethernet
-  SMSC LAN8710/LAN8720 PHY

::

                  +-------------------------------+
                  |          TI AM335X            |
                  |        ARM Cortex A8          |
                  |                               |
                  +---------------+---------------+
                                  |
                              +---+---+
                              | sw0p2 |
                  +-----------+-------+-----------+
                  |             [cpu]             |
                  |                               |
                  |      TI CPSW Switch driver    |
                  |                               |
                  |   internal            <eth1 > |
                  +--+-------+----------+-------+-+
                     | sw0p0 |          | sw0p1 |
                     +---+---+          +---+---+
                         |
                     +---+---+
                     | sw1p4 |
    +----------------+-------+---------------------+
    |                   cpu                        |
    |                                              |
    |   NXP SJA1105TEL switchdev driver            |
    |                                              |
    | umnet0      broadr0      broadr1       eth0  |
    +-------+----+-------+----+-------+----+-------+
    | sw1p0 |    | sw1p1 |    | sw1p2 |    | sw1p3 |
    +---+---+    +---+---+    +---+---+    +---+---+
        |            |            |            |
        |            |            |            |
        |            |            |            |
    +---+----+   +---+---+    +---+---+    +---+---+
    | User   |   |  phy6 |    |  phy7 |    |  phy1 |
    | Module |   +---+---+    +---+---+    +---+---+
    |        |       |            |            |
    |        |   +------------------+   +-----------------+
    |        |   | NXP TJA110X      |   | SMSC LAN87X     |
    |        |   | driver           |   | driver          |
    +--------+   +------------------+   +-----------------+

NXP SJA1105TEL switchdev driver
-------------------------------

.. note::
   Changes between kernel 4.19 and kernel 5.10:

     * Interfaces have been renamed:

       * eth0 => internal (no need to configure it, it just needs to be up)
       * lan0 => eth0
       * eth0* (removed)

     * The "physical" interfaces (eth0, broadr0, broadr1, etc) can be configured directly
     * VLAN tagging is handled by the driver and there is no need to care about it
     * bridging can be configured directly through NetworkManager

.. warning::
   Putting two interfaces of the switch in the same subnet will lead to networking
   problems. There should anyway be no reason for such a setup except for testing.

The SJA1105TEL switchdev driver functionality:
- Ports are available in user space as netdevs (e.g. broadr1, eth0)
- Hardware statistics and standard information about devices (ethtool)
- Rx/Tx and error statistics (ifconfig)
- Hardware offloading (bridge fdb)
- VLAN tagging/bridging support (brctl, bridge vlan)

The SJA1105TEL switchdev driver limitations:
- No VLAN double tagging

Hardware offloading
~~~~~~~~~~~~~~~~~~~

The idea is to offload the L2 data forwarding (switching) path from the
kernel to the switch device by mirroring bridge FDB entries down to the
device. An FDB entry is the {port, MAC, VLAN} tuple forwarding
destination.

Static bridge FDB entries are installed, for example, using bridge
command:

::

    $ bridge fdb add ADDR dev DEV [vlan VID] [self]

The SJA1105TEL switch device has 1024 entries in the L2 address lookup
table. Parts of the table can be statically configured, e.g.:

.. code-block:: shell-session

    $ bridge fdb add 00:1b:21:2f:fa:1f dev eth0

Some of the entries are dynamically learned during operation. Show all
valid FDB entries:

.. code-block:: shell-session

    $ bridge fdb show

Loaded entries never time-out and cannot be replaced by learned entries.
They have to be removed manually, e.g.:

.. code-block:: shell-session

    $ bridge fdb del 00:1b:21:2f:fa:1f dev eth0

Bridging
~~~~~~~~

Two or more interfaces of the switch can be bridged together. This means
that devices on these two interfaces will be able to communicate as if
they were directly connected, without, the CPU having to route the packets.

.. warning::
   stp must never been enabled when bridging two ports of the switch,
   therefore, two ports should also never be put in the same subnet and
   loops must be avoided manually.

   If a loop is created or if stp is enabled, the switch will not behave
   as expected and other interfaces which may not be involved in the
   bridge can also stop working properly.

E.g. Bridging eth0 and broadr0:

.. code-block:: shell-session

    nmcli con add type bridge ifname br0 con-name br0 bridge.stp no
    nmcli con add type bridge-slave ifname eth0 master br0
    nmcli con add type bridge-slave ifname broadr0 master br0
    nmcli con up br0

By default, NetworkManager enables DHCP on the created bridge. If the two
interface must just be bridged without interaction with linux, ipv4 and
ipv6 can be disabled.

.. code-block:: shell-session

    nmcli con add type bridge ifname br0 con-name br0 bridge.stp no
    nmcli con modify br0 ipv4.method disable ipv6.method disabled
    nmcli con add type bridge-slave ifname eth0 master br0
    nmcli con add type bridge-slave ifname broadr0 master br0
    nmcli con up br0


NXP TJA110X PHY driver
----------------------

.. note::
   Changes between kernel 4.19 and kernel 5.10:

   * Configuration is now done through ethtool instead of sysfs


Checking link status of a TJA1100 PHY.

E.g. broadr0, cable connected:

.. code-block:: shell-session

   ethtool broadr0 | grep -i "link detected"
           Link detected: yes


E.g. broadr1, cable disconnected:

.. code-block:: shell-session

    ethtool broadr1 | grep -i "link detected"
            Link detected: no

Both TJA110X PHYs are configured as slaves by default.

E.g. check master/slave configuration of broadr0, default configuration:

.. code-block:: shell-session

    ethtool broadr0 | grep -i "master-slave"
            master-slave cfg: forced slave


E.g. set broadr1 as master (a counterpart has to be set as slave):

.. code-block:: shell-session

    ethtool -s broadr1 master-slave forced-master
    ethtool broadr1 | grep -i "master-slave"
            master-slave cfg: forced master


The TJA1100 PHY driver can give SNR class of a connection.

E.g. check connection quality of broadr0 to a counterpart over short
good quality cable:

.. code-block:: shell-session

    ethtool broadr0 | grep -i sqi
            SQI: 7/7

E.g. check connection quality of broadr1 to a counterpart over long poor
quality cable:

.. code-block:: shell-session

    ethtool broadr1 | grep -i sqi
            SQI: 1/7

User space tools and configuration
----------------------------------

Check available interfaces:

.. code-block:: shell-session

    $ ip link show

List IP addresses:

.. code-block:: shell-session

    $ ip address show

List routes:

.. code-block:: shell-session

    $ ip route show

Deactivate a link layer device:

.. code-block:: shell-session

    $ ip link set dev devicename down

Activate a device:

.. code-block:: shell-session

    $ ip link set dev devicename up

Print current settings of the specified device

.. code-block:: shell-session

    $ ethtool devname

Print statistics of the specified device

.. code-block:: shell-session

    $ ethtool -S devname

References
----------

-  `kernel.org <https://www.kernel.org>`_

-  `dsa <https://www.kernel.org/doc/Documentation/networking/dsa/dsa.txt>`_
-  `phy <https://www.kernel.org/doc/Documentation/networking/phy.txt>`_
-  `netdevices <https://www.kernel.org/doc/Documentation/networking/netdevices.txt>`_
-  `switchdev <https://www.kernel.org/doc/Documentation/networking/switchdev.txt>`_
