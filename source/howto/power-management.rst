Power Management
================

Overview
--------

NetModule OEM Linux Distribution provides two standard high-level power
management strategies:

-  system-wide power management,
-  working-state power management.

System-wide power management strategy is using global low-power states
to reduce system activity. In these states, referred as sleep states,
user space code cannot be executed. Depending on sleep state supported
by the platform, different levels of energy saving can be achieved. To
get back to the working state, the system expects to receive a special
signal from one of the designated devices.

Working-state power management strategy corresponds to adjusting the
power states of individual hardware components.

Supported hardware
~~~~~~~~~~~~~~~~~~

-  DA9063 System PMIC

Supported sleep states
~~~~~~~~~~~~~~~~~~~~~~

Standby
  This state provides a relatively straightforward transition back to the
  working state. In this state the system core logic retains power and no
  operating state is lost. It offers moderate, real energy savings.

PMIC's Power Down Mode
  Platforms with DA9063 System PMIC can support more sophisticated energy
  saving options by disabling the system power domain. The system power
  domain is enabled on a signal received from a preconfigured wake-up
  device.

Supported wake-up scenarios
~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  RTC based alarm
-  IMU based events: single-tap, double-tap
-  ONKEY event
-  KL15 event

Smart Battery
-------------

NetModule OEM Linux Distribution can run on devices supplied by a smart
battery. To get current status of the battery our distribution provides
a battery test tool.

User space tools and configuration
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Enter standby state:

::

    $ echo standby > /sys/power/state

Wake up from standby after 10 seconds:

::

    rtcwake -d /dev/rtc0 -m standby -s 10

or without rtcwake in your image:

::

    echo +10 > /sys/class/rtc/rtc0/wakealarm && echo standby > /sys/power/state

Enter PMIC's power down mode:

::

    $ poweroff

Start system after ~1 min when in PMIC's power down mode:

::

    echo +60 > /sys/class/rtc/rtc0/wakealarm && poweroff

Using a battery test:

::

    $ batterytest -h
    battery tool

    Examples:
      batterytest -v         get voltage
      batterytest -a         get current
      batterytest -t         do battery test

    Main modes of operation:
     -h, --help              print this help
     -A, --all               print all battery information
     -v, --voltage           get battery voltage
     -a, --current           get battery current
     -c, --capacity          get remaining capacity
     -r, --reg               raw mode, register access
     -t, --test              run battery test
