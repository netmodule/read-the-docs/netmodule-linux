.. _fw-update:

Updating firmware
=================

The NetModule Linux system provides a tool to upgrade the firmware of the
different modules in the system.

The firmware packages are not available in the reference image but the latest supported
firmware versions are part of each software release.



Firmware Location
-----------------

The firmwares officialy supported by NetModule are available at https://nmrepo.netmodule.com/chbe/fwupdate/


Preparation
-----------

Before upgrading a module, it is recommended to check if it has been properly
configured and started. All modules have matching service which is fetching
the relevant informations from this module. See module specific examples below
to see how to check the service for each type of module.

When the modules are properly configured, the output shows one of the
two following lines:

.. code-block:: bash

   Active: active (running)
   Active: active (exited)

Once the device is started, the firmware version can be read under the following path:
`/run/<module type>/<module>.config`

See module specific examples below.


Usage
-----

The update tool can fetch the new firmware from the local device or via http
from a remote server.


Local firmware
~~~~~~~~~~~~~~

To upgrade using a local firmware, the firmware must be copied to the target first.
The following command can then be executed:

.. code-block:: bash

   nmhw-fwupdate <module> <firmware.tar.gz>

Where <module> is the module name as reported in the ".config" filename
under `/run/<module type>/<module>.config` and <firmware> is the relative or
full path to the firmware archive. See module specific examples below.


Remote firmware (HTTP)
~~~~~~~~~~~~~~~~~~~~~~

If the firmware is available on an HTTP server (reachable from the target),
the following command can be used:

.. code-block:: bash

   nmhw-fwupdate <module> <http link>


Where <module> is the module name as reported in the ".config" filename
under `/run/<module type>/<module>.config` and <http link> is the link to
the firmware on the HTTP server. See module specific examples below.


Bootloaders
-----------
.. warning::
    Upgrading a bootloader is a critical step and incompatibilities between
    a bootloader and the linux version in use may break the system.
    The upgrade should NOT be done without detailed instructions.

Supported systems
~~~~~~~~~~~~~~~~~

- HW21 (u-boot)
- HW26 (u-boot)
- HW23 (imx-boot)

Example
~~~~~~~~
.. code-block:: bash

    # Check service status
    systemctl status bootloader-config

    # Check bootloader type and version
    cat /run/bootloader/bootloader0.config

    # Update from local firmware
    nmhw-fwupdate bootloader0 nmhw21-u-boot-1.2.2.tar.gz


WWAN
----

.. warning:: Downgrading a WWAN modem will likely lead to a broken modem

Supported modules
~~~~~~~~~~~~~~~~~
- u-blox TOBY-L210
- u-blox LARA-L6004
- u-blox LARA-L6004D

Example
~~~~~~~

.. code-block:: bash

    # Check service status
    systemctl status wwan-config@wwan0

    # Check wwan0 type and version
    cat /run/wwan/wwan0.config

    # Update from local firmware
    nmhw-fwupdate wwan0 ublox-toby-l210_17.00-A01.01.tar.gz

    # Update from remote firmware
    nmhw-fwupdate wwan0 https://nmrepo.netmodule.com/chbe/fwupdate/ublox-toby-l210_17.00-A01.01.tar.gz


GNSS
----

Supported modules
~~~~~~~~~~~~~~~~~
- u-blox NEO-M8L

Example
~~~~~~~
.. code-block:: bash

    # Check service status
    systemctl status gnss-mgr

    # Check gnss0 type and version
    cat /run/gnss/gnss0.config

    # Update from local firmware
    nmhw-fwupdate gnss0 UBX_M8_301_ADR_431_NEO_M8L.tar.gz

    # Update from remote firmware
    nmhw-fwupdate gnss0 https://nmrepo.netmodule.com/chbe/fwupdate/UBX_M8_301_ADR_431_NEO_M8L.tar.gz



Creating Bootloader Update Package
==================================

Preparation
-----------
Make sure you have the following binaries available:

* The bootloader images:

    - for am335x devices:

        + spl-u-boot-am335x-<hwType>.img
        + u-boot-am335-<hwType>.img

    - for imx8 devices:

        + imx-boot

* the flasher and helper binaries, which you can extract from a previously gotten bootloader update package otherwise call your NetModule Contact

    - for am335x devices:

        + flasher-netbird
        + check_uboot_header.sh

    - for imx8 devices:

        + flasher-net64


Create the Package
------------------
The update package is a simple gzipped tarball of the following content:

* for am335x devices:

    ::

        e.g. for a HW24
        .
        ├── checksums
        ├── check_uboot_header.sh
        ├── flasher-netbird
        ├── info
        ├── spl-u-boot-am335x-nmhw24.img
        └── u-boot-am335x-nmhw24.img

* for imx8 devices:
    
    ::

        .
        ├── checksums
        ├── flasher-net64
        ├── img
        │   └── imx-boot
        └── info


Follow the steps below and use the binaries as described before and you will get your own update package:

1. create a directory, e.g. ``fw-pkg``
2. copy the bootloader and flasher binaries into this directory

     NOTE: For imx8 devices the bootloader binary goes into a sub-directory named ``img``

3. create the checkum file ``checksums`` with the following syntax:

    - for am335x devices:

        ::

            <md5-sum>  <spl-image-name>
            <md5-sum>  <u-boot-image-name>

    - for imx8 devices:

        ::

            <md5-sum>  img/<bootloader-image-name>

4. create an information file ``info`` with the following content, whereas the ``compatible`` string uses the HW numbers as defined as follows:

    * until HW20: ``nrhw<HW-Nbr>``

        - example for HW20:  ``nrhw20``

    * after HW20 to HW24: ``nmhw<HW-Nbr>``
    
        - example for HW24: ``nmhw24``

    * after HW24: ``hw<HW-Nbr>``
    
        - example for HW26: ``hw26``

    ::

        version: <version>
        type: bootloader
        family: bootloader
        compatible: <hw-compatible-string>

    for a HW24 with version 1.3.0 you will have this version file:

        ::

            version: 1.3.0
            type: bootloader
            family: bootloader
            compatible: nmhw24

5. create the package of the entire content

    * pack the content using tar: ``tar czf <package-name>.tar.gz -C fw-pkg .``

        - example for HW24 and version 1.3.0: ``tar czf hw24-bootloader-1.3.0.tar.gz -C fw-pkg .``

