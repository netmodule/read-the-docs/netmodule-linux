Booting with custom Linux kernel or ramdisk
===========================================

If it's necessary, it is possible to boot custom a Linux kernel with
existing OSTree controlled file system.

Provisioning over tftp
----------------------

1. Load the ostree necessary variables.

::

  run bootcmd_otenv

2. Load your own kernel.

::

  tftp $kernel_addr_r fitImage

xor load the installed kernel:

::

  ext4load mmc 1:1 $kernel_addr_r /boot$kernel_image

3. Load your own ramdisk.

::

  tftp $ramdisk_addr_r ramdisk

xor load the installed ramdisk:

::

  `ext4load mmc 1:1 \$ramdisk_addr_r /boot\$ramdisk_image;

4. Boot the system.

::

  bootm $kernel_addr_r $ramdisk_addr_r

Provisioning over USB
---------------------

1. Initialize USB

::

  usb reset

2. Load the ostree necessary variables.

::

  run bootcmd_otenv

3. Load your own kernel.

::

  fatload usb 0:1 $kernel_addr_r fitImage

xor load the installed kernel:

::

  ext4load mmc 1:1 $kernel_addr_r /boot$kernel_image

4. Load your own ramdisk.

::

  fatload usb 0:1 $ramdisk_addr_r ramdisk

xor load the installed ramdisk:

::

   ext4load mmc 1:1 $ramdisk_addr_r /boot$ramdisk_image;

5. Boot the system.

::

  bootm $kernel_addr_r $ramdisk_addr_r
