Create a fitImage
=================

To create a fitImage you need a .its file. Luckily YoctoProject creates this
file for us. You find in the deployed image folder (e.g.
``hancock-os/shared-build/tmp/deploy/images/am335x-nmhw21/fitImage-its-am335x-nmhw21.its``).

In this file, you will find this line in the kernel section:

::

  data = /incbin/("linux.bin");

This tells us what the filename of your legacy kernel (zImage, uImage, Image) needs to be called.

You will also find this line in the dtb section:

::

  data = /incbin/("arch/arm/boot/dts/am335x-nmhw21-prod1.dtb");

This tells us where the the dtb is expected.

Steps (nmhw21)
--------------

1. Create a folder structure that looks like this:

::

  |-- fitImage-its-am335x-nmhw21.its
  |
  |-- linux.bin ( --> your kernel image)
  |
  |-- arch/arm/boot/dts/am335x-nmhw21-prod1.dtb

2. To create the fitimage now run in your folder:

::

  mkimage -f fitImage-its-am335x-nmhw21.its fitImage
