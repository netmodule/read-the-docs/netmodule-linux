.. NetModule OEM Linux Distribution documentation master file, created by
   sphinx-quickstart on Thu May 16 15:13:55 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to NetModule OEM Linux Distribution's documentation!
============================================================

For more than 15 years NetModule has proven itself as a reliable OEM
partner with thousands of installed devices for customers all over the
world!

NetModule OEM platforms come with the feature-rich NetModule Router
Software or a standard Yocto Linux for customer-specific applications.

Contents:

.. toctree::
   :caption: About
   :maxdepth: 2
   :glob:

   about/*

.. toctree::
   :caption: Features
   :maxdepth: 2
   :glob:

   features/*

.. toctree::
   :caption: Getting Started
   :maxdepth: 2
   :glob:

   gettingstarted/*

.. toctree::
   :caption: Binaries
   :maxdepth: 2
   :glob:

   artifacts/*

.. toctree::
   :caption: System Startup
   :maxdepth: 2
   :glob:

   startup/*

.. toctree::
   :caption: How-To
   :maxdepth: 4
   :glob:

   howto/*

.. toctree::
   :caption: Development
   :maxdepth: 3
   :glob:

   development/*

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

